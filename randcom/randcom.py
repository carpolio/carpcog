from discord.ext import commands
import random
import json


class randcom:
    """My custom cog that does stuff!"""

    def __init__(self, bot):
        self.bot = bot
        try:
            comFile = open("./cogs/randcomList.json", mode='r')
            self.comlist = json.load(comFile)
            comFile.close()
        except FileNotFoundError:
            self.comlist = {}
        except OSError:
            print("Randcom could not open or create command list file")

    async def bot_say(self, message):
        await self.bot.say(message)

    async def add_command(self, arg1, arg2):

        if arg1 != "":
            if arg1 in self.comlist:
                self.comlist[arg1].append(arg2)
            else:
                self.comlist[arg1] = [arg2]
            try:
                comFile = open("./cogs/randcomList.json", mode='w')
                json.dump(self.comlist, comFile)
                comFile.close()
            except OSError:
                print("There was an issue writing the file")
        else:
            await self.bot.say("ERROR: Must supply a list to add to")

    async def list_command(self, context, arg1):
        caller = context.message.author
        if arg1 == "":
            await self.bot.say("Stored Lists: ")
            for key in self.comlist.keys():
                await self.bot.say(key)
        else:
            element_num = 0
            for entry in self.comlist[arg1]:
                await self.bot.send_message(caller, str(element_num) + ".\t<" + entry + ">")
                element_num += 1

    async def run_command(self, context, arg1):
        channel = context.message.channel
        if arg1 == "":
            await self.bot.say(random.choice(self.comlist[random.choice(list(self.comlist.keys()))]))
        else:
            await self.bot.send_message(channel, random.choice(self.comlist[arg1]))

    async def del_command(self, context, arg1, arg2):
        caller = context.message.author
        if arg1 != "":
            if arg2 == "":
                element_num = 0
                for entry in self.comlist[arg1]:
                    await self.bot.send_message(caller, str(element_num) + ".\t<" + entry + ">")
                    element_num += 1
            else:
                listlength = len(self.comlist[arg1])
                if int(arg2) < listlength:
                    await self.bot.say(self.comlist[arg1].pop(int(arg2)) + " has been removed")
                    try:
                        comFile = open("./cogs/randcomList.json", mode='w')
                        json.dump(self.comlist, comFile)
                        comFile.close()
                    except OSError:
                        print("There was an issue writing the file")
                else:
                    await self.bot.say("Invalid choice please pick a number less than " + str(listlength))
        else:
            await self.bot.say("Please choose a list to display")

    @commands.command(pass_context=True)
    async def randcom(self, ctx, cmd, arg1="", arg2=""):
        """cmd - add,list or run
        randcom add arg1 arg2  - adds command arg2 to list arg1
        randcom list           -  display all lists and commands
        randcom list arg1      - display all commands in list arg1
        randcom run arg1       - randomly run a command in list arg1
        randcom run            - randomly choose a command from all lists
        randcom del arg1       - displays list of commands with numbers for deletion
        randcom del arg1 arg2  - remove an item in the supplied list arg1"""

        cmd = cmd.lower()
        if cmd == "add":
            await self.add_command(arg1, arg2)
        elif cmd == "list":
            await self.list_command(ctx, arg1)
        elif cmd == "run":
            await self.run_command(ctx, arg1)
        elif cmd == "del":
            await self.del_command(ctx, arg1, arg2)
        else:
            await self.bot_say("randcom: Invalid command")

    @commands.command()
    async def rcloadlist(self, repo_name):
        """Loads the list downloaded from the randcom repo"""
        repo_name = repo_name.lower()
        if repo_name != "":
            try:
                comFile = open("./data/downloader/" + str(repo_name) + "/randcom/randcomList.json", mode='r')
                self.comlist = json.load(comFile)
                comFile.close()
            except FileNotFoundError:
                await self.bot.say("Could not load list file")
            except OSError:
                print("Randcom could not open or create command list file")
            try:
                comFile = open("./cogs/randcomList.json", mode='w')
                json.dump(self.comlist, comFile)
                comFile.close()
            except OSError:
                print("There was an issue writing the file")
        else:
            await self.bot.say("Please provide the cog <repo-name>")

    @commands.command(pass_context=True)
    async def exportList(self, ctx):
        """Export the Randcom JSON file"""
        try:
            caller = ctx.message.author
            forexport = open("./cogs/randcomList.json", mode='r')
            await self.bot.send_file(caller, forexport, filename="randcomList.json", content="Full command list",
                                     tts=False)
            forexport.close()
        except FileNotFoundError:
            await self.bot.say("Could not find the Randcom list for export")
        except OSError:
            print("Randcom could not open file for export")


def setup(bot):
    bot.add_cog(randcom(bot))
